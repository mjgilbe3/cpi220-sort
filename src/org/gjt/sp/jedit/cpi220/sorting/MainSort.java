package org.gjt.sp.jedit.cpi220.sorting;

import org.gjt.sp.jedit.cpi220.performance.Stopwatch;

public class MainSort {

	public static void main(String[] args) {
		
		// Read CSV
		Surname[] surnames = IO.read("surnames.csv");
		
		// Size of the Array
		int length = surnames.length;
		
		// Optional Shuffle
		Shuffle.shuffle(surnames);
		
		// Add stopwatch here
		Stopwatch totalTime = new Stopwatch();
		
		// Sort
		System.out.println("Starting sorting...");
		Insertion.sort(surnames);
		
		// Stop stopwatch
		double time = 0d; // Stop watch here
		time = totalTime.elapsedTime();
		System.out.println("Elapsed time:" + time);
		
		// Write to CSV
		IO.write(surnames, "output.csv");
	}
	
}
